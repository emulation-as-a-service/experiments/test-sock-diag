#!/bin/sh

if ! test "$_TEST_SOCK_DIAG_UNSHARED"; then
  _TEST_SOCK_DIAG_UNSHARED=1 exec unshare -rm "$0" "$@"
fi

TMPDIR="$(mktemp -d)"

mkdir "$TMPDIR/lower" "$TMPDIR/upper" "$TMPDIR/work" "$TMPDIR/mount"
mount -t tmpfs tmpfs "$TMPDIR/lower"
mount -t overlay \
  -o "$MOUNTOPTS" \
  -o "lowerdir=$TMPDIR/lower,upperdir=$TMPDIR/upper,workdir=$TMPDIR/work" \
  overlay "$TMPDIR/mount"

./test-sock-diag "$TMPDIR/mount/test.sock"

mount | grep -F -- "$TMPDIR"
stat "$TMPDIR/"*
