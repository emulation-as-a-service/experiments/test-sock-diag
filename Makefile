all: test-sock-diag

test-sock-diag: test-sock-diag.c
	$(CC) $(CFLAGS) -static -o $@ $<
	printf 'git-commit: %s\0' $(shell git describe --dirty --all --abbrev=128 --long --always) > .git-commit
	objcopy --add-section .note.git-commit=.git-commit $@
