# test-sock-diag

## Inodes on overlay filesystems

Inodes on overlay filesystems are not necessarily persistent. This means that
[sock_diag/UDIAG_SHOW_VFS](https://man7.org/linux/man-pages/man7/sock_diag.7.html)
cannot report proper inode/device numbers for UNIX domain sockets on these
filesystems. This also means that CRIU cannot freeze processes using UNIX domain sockets
on such filesystems. 

For more information, read
<https://github.com/torvalds/linux/blob/master/Documentation/filesystems/overlayfs.rst>.

`./test-sock-diag /somepath/file.socket`

tests if sock_diag/UDIAG_SHOW_VFS reports proper information for sockets in `/somepath/`.

## Kernel module `unix_diag` (`net-pf-16-proto-4-type-1`)

`sock_diag` can only work for UNIX domain sockets at all if the kernel module
[`unix_diag`](https://github.com/torvalds/linux/blob/master/net/unix/diag.c)
(also known as `net-pf-16-proto-4-type-1`) can be loaded by the
kernel. The kernel can load this automatically via
[`__request_module`](https://github.com/torvalds/linux/blob/master/kernel/kmod.c#L125)
if it is not loaded. It has to be present as a module (`unix_diag.ko`, `modinfo unix_diag`)
(`Kconfig`: `CONFIG_UNIX_DIAG=m`) on your system, though.

---

Unrelatedly, `ss -axe` incorrectly decodes `udiag_vfs_dev` in
[`unix_show_sock()`](https://git.kernel.org/pub/scm/network/iproute2/iproute2.git/tree/misc/ss.c#n3909).
CRIU handles this correctly, though, in [`kdev_to_odev`](https://github.com/checkpoint-restore/criu/blob/HEAD/criu/include/util.h#L153)
(`(struct unix_diag_vfs).udiag_vfs_dev` (`MMMmmmmm`) has a different representation
than `(struct stat).st_dev` (`mmmMMMmm`) for the 32-bit device number with
its 12-bit major (`MMM`, 3 hex digits) and 20-bit minor (`mmmmm`, 5 hex digits) number.)